package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.api.IServiceLocator;
import ru.bokhan.tm.api.endpoint.IServerInfoEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
@AllArgsConstructor
@NoArgsConstructor
public class ServerInfoEndpoint implements IServerInfoEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @Override
    @NotNull
    @WebMethod
    public String getServerHost() {
        return serviceLocator.getPropertyService().getServerHost();
    }

    @Override
    @NotNull
    @WebMethod
    public Integer getServerPort() {
        return serviceLocator.getPropertyService().getServerPort();
    }

}
