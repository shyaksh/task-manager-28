package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IServiceLocator;
import ru.bokhan.tm.api.repository.ITaskRepository;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.dto.TaskDTO;
import ru.bokhan.tm.exception.empty.EmptyDescriptionException;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyNameException;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIndexException;
import ru.bokhan.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskService extends AbstractService<TaskDTO, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository() {
        @NotNull final EntityManager em = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(em);
        return repository;
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        persist(task);
    }

    @Override
    public void create(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDescription(description);
        persist(task);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final TaskDTO taskDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskDTO == null) throw new EmptyUserIdException();
        taskDTO.setUserId(userId);
        persist(taskDTO);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final TaskDTO taskDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskDTO == null) return;
        taskDTO.setUserId(userId);
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.remove(taskDTO);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ITaskRepository repository = getRepository();
        @NotNull final List<TaskDTO> result = repository.findAll(userId);
        return result;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.clear(userId);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository repository = getRepository();
        @Nullable final TaskDTO result = repository.findById(userId, id);
        return result;
    }

    @Nullable
    @Override
    public TaskDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final ITaskRepository repository = getRepository();
        @Nullable final TaskDTO result = repository.findByIndex(userId, index);
        return result;
    }

    @Nullable
    @Override
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository repository = getRepository();
        @Nullable final TaskDTO result = repository.findByName(userId, name);
        return result;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeById(userId, id);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeByIndex(userId, index);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskRepository repository = getRepository();
        try {
            repository.begin();
            repository.removeByName(userId, name);
            repository.commit();
        } catch (@NotNull final Exception e) {
            repository.rollback();
            e.printStackTrace();
        } finally {
            repository.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskDTO task = findById(userId, id);
        if (task == null) throw new IncorrectIdException();
        task.setName(name);
        task.setDescription(description);
        merge(task);
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskDTO task = findByIndex(userId, index);
        if (task == null) throw new IncorrectIndexException();
        task.setName(name);
        task.setDescription(description);
        merge(task);
    }

}