package ru.bokhan.tm.exception.system;

public final class NotFoundException extends RuntimeException {

    public NotFoundException() {
        super("Error! No such Object in database...");
    }

}
