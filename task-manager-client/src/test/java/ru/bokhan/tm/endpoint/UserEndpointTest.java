package ru.bokhan.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.bokhan.tm.marker.IntegrationCategory;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static ru.bokhan.tm.constant.UserTestData.USER1;
import static ru.bokhan.tm.constant.UserTestData.USER2;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final UserEndpoint endpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    private SessionDTO adminSession;

    private SessionDTO userSession;


    @Before
    public void setUp() {
        adminSession = sessionEndpoint.openSession("admin", "admin");
        userSession = sessionEndpoint.openSession("user", "user");
        domainEndpoint.saveToJson(adminSession);
    }

    @After
    public void tearDown() {
        domainEndpoint.loadFromJson(adminSession);
        adminSession = sessionEndpoint.openSession("admin", "admin");
        sessionEndpoint.closeSessionAll(adminSession);
    }

    @Test
    public void createUser() {
        endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        @NotNull final UserDTO actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
    }

    @Test(expected = Exception.class)
    public void createUserByNotAdminNegative() {
        endpoint.createUser(userSession, USER1.getLogin(), USER1.getLogin());
    }

    @Test
    public void createUserWithEmail() {
       endpoint.createUserWithEmail(
                adminSession, USER1.getLogin(), USER1.getLogin(), USER1.getEmail()
        );
        @NotNull final UserDTO actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
        Assert.assertEquals(USER1.getEmail(), actual.getEmail());
    }

    @Test
    public void registerUserWithEmail() {
        endpoint.registerUserWithEmail(
                USER1.getLogin(), USER1.getLogin(), USER1.getEmail()
        );
        @NotNull final UserDTO actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
        Assert.assertEquals(USER1.getEmail(), actual.getEmail());
    }

    @Test
    public void createUserWithRole() {
       endpoint.createUserWithRole(
                adminSession, USER1.getLogin(), USER1.getLogin(), USER1.getRole()
        );
        @NotNull final UserDTO actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
        Assert.assertEquals(USER1.getRole(), actual.getRole());
    }

    @Test
    public void findUserById() {
        @NotNull final UserDTO expected = endpoint.findUserByLogin(adminSession, "admin");
        Assert.assertNotNull(expected);
        @NotNull final UserDTO actual = endpoint.findUserById(adminSession, expected.getId());
        Assert.assertNotNull(actual);
        assertThat(actual).isEqualToComparingFieldByField(expected);
    }

    @Test
    public void findUserByLogin() {
        endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        @NotNull final UserDTO actual = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        Assert.assertEquals(USER1.getLogin(), actual.getLogin());
    }

    @Test
    public void removeUserById() {
        @NotNull final UserDTO expected = endpoint.findUserByLogin(adminSession, "user");
        Assert.assertNotNull(expected);
        endpoint.removeUserById(adminSession, expected.getId());
        Assert.assertNull(endpoint.findUserById(adminSession, "user"));
    }

    @Test
    public void removeUserByLogin() {
        @NotNull final UserDTO expected = endpoint.findUserByLogin(adminSession, "user");
        Assert.assertNotNull(expected);
        endpoint.removeUserByLogin(adminSession, expected.getLogin());
        Assert.assertEquals(expected.getLogin(), "user");
        Assert.assertNull(endpoint.findUserById(adminSession, "user"));
    }

    @Test
    public void updateUser() {
        endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        @NotNull final SessionDTO newUserSession = sessionEndpoint.openSession(USER1.getLogin(), USER1.getLogin());
        endpoint.updateUser(
                newUserSession,
                USER2.getLogin(),
                USER2.getFirstName(),
                USER2.getLastName(),
                USER2.getMiddleName(),
                USER2.getEmail()
        );
        @NotNull final UserDTO updated = endpoint.findUserById(adminSession, USER1.getId());
        assertThat(updated).isEqualToIgnoringGivenFields(USER2, "id", "passwordHash", "locked");
    }

    @Test
    public void updateUserById() {
        endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        @NotNull final SessionDTO newUserSession = sessionEndpoint.openSession(USER1.getLogin(), USER1.getLogin());
        endpoint.updateUserById(
                adminSession,
                USER1.getId(),
                USER2.getLogin(),
                USER2.getFirstName(),
                USER2.getLastName(),
                USER2.getMiddleName(),
                USER2.getEmail()
        );
        @NotNull final UserDTO updated = endpoint.findUserById(adminSession, USER1.getId());
        assertThat(updated).isEqualToIgnoringGivenFields(USER2, "id", "passwordHash", "locked");
    }

    @Test
    public void updateUserPassword() {
        @NotNull final String password = USER1.getLogin();
        endpoint.createUser(adminSession, USER1.getLogin(), password);
        @NotNull final SessionDTO newUserSession = sessionEndpoint.openSession(USER1.getLogin(), password);
        @NotNull final String passwordNew = USER2.getLogin();
        endpoint.updateUserPassword(newUserSession, passwordNew);
        sessionEndpoint.closeSession(newUserSession);
        Assert.assertNotNull(sessionEndpoint.openSession(USER1.getLogin(), passwordNew));
    }

    @Test
    public void updateUserPasswordById() {
        @NotNull final String password = USER1.getLogin();
        endpoint.createUser(adminSession, USER1.getLogin(), password);
        @NotNull final String passwordNew = USER2.getLogin();
        endpoint.updateUserPasswordById(adminSession, USER1.getId(), passwordNew);
        Assert.assertNotNull(sessionEndpoint.openSession(USER1.getLogin(), passwordNew));
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final String password = USER1.getLogin();
        endpoint.createUser(adminSession, USER1.getLogin(), password);
        endpoint.lockUserByLogin(adminSession, USER1.getLogin());
        Assert.assertNull(sessionEndpoint.openSession(USER1.getLogin(), password));
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final String password = USER1.getLogin();
        endpoint.createUser(adminSession, USER1.getLogin(), password);
        endpoint.lockUserByLogin(adminSession, USER1.getLogin());
        Assert.assertNull(sessionEndpoint.openSession(USER1.getLogin(), password));
        endpoint.unlockUserByLogin(adminSession, USER1.getLogin());
    }

    @Test
    public void findUserAll() {
        @NotNull final List<UserDTO> actual = endpoint.findUserAll(adminSession);
        actual.forEach(user -> assertThat(user).isInstanceOf(UserDTO.class));
        Assert.assertEquals(2, actual.size());
    }

    @Test
    public void removeUser() {
       endpoint.createUser(adminSession, USER1.getLogin(), USER1.getLogin());
        Assert.assertNotNull(endpoint.findUserById(adminSession, USER1.getId()));
        @NotNull UserDTO created = endpoint.findUserByLogin(adminSession, USER1.getLogin());
        endpoint.removeUser(adminSession,created );
        Assert.assertNull(endpoint.findUserById(adminSession, USER1.getId()));
    }

}